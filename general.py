#encoding:UTF-8

# This file is part of basicutils
# 
# basicutils is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# basicutils is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with basicutils.  If not, see <http://www.gnu.org/licenses/>.

'''
Created on 26 Oct 2012

@author: enrico
'''
from collections import deque
import itertools
from datetime import datetime, timedelta
from time import mktime

def total_seconds(td):
    return (td.microseconds + (td.seconds + td.days * 24 * 3600) * 10**6) / 10**6

def to_timestamp(dt):
    return mktime( dt.timetuple() )

def to_datetime(timestamp):
    return datetime.fromtimestamp(timestamp)

def floor_30(dt):
    return dt - timedelta(minutes= dt.minute % 30, 
                          seconds=dt.second, 
                          microseconds = dt.microsecond)

def floor_15(dt):
    return dt - timedelta(minutes= dt.minute % 15, 
                          seconds = dt.second, 
                          microseconds=dt.microsecond)

def round_30(dt):
    timestamp = to_timestamp(dt)
    rounded = round(timestamp / (30 * 60)) * 30 * 60
    return datetime.fromtimestamp(rounded)

def round_15(dt):
    timestamp = to_timestamp(dt)
    rounded = round(timestamp / (15 * 60)) * 15 * 60
    return datetime.fromtimestamp(rounded)
    
def moving_average(iterable, n=3):
    it = iter(iterable)    
    d = deque(itertools.islice(it, n-1))
    d.appendleft(1.0)
    s = sum(d)
    for elem in it:
        s += elem - d.popleft()
        d.append(elem)
        yield s / float(n)

